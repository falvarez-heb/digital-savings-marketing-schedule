# FX's Digital savings marketing schedule

This repo pulls data from Airtable and it creates Slack reminders.

The cron job is run daily at 3pm and looks for new entries.
---

## Running Locally

Use `npm run start_local` to load environment variables from `.env` file.

- Note, the `.env` file is not pushed to the git repo.

---

## Env Vars

Here's how the `.env` file should look like:

    # .env
    AIRTABLE_API_KEY = "keyxxxxxxxxxxxxxx"
    AIRTABLE_BASE_ID = "idFromUrlOnAppPageOfBase"
    AIRTABLE_BASE_NAME = "Digital savings marketing schedule"
    AIRTABLE_VIEW_NAME = "pending-offers"

    SLACK_BOT_USER_OAUTH_TOKEN = "xoxb-xxxxxxxxxxxxx-xxxxxxxxxxxxx-xxxxxxxxxxxxxxxxxxxxxxxx"

## Offer example
```
{
  description: 'OB Brand Builder',
  communicationChannel: 'Email',
  audienceSize: 35000,
  offerType: 'Digital coupon',
  unixTimestamp: 1625614200
}
```