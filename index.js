"use strict";

const Airtable = require("airtable");
const { WebClient } = require("@slack/web-api");
const {
  AIRTABLE_API_KEY,
  AIRTABLE_BASE_ID,
  SLACK_BOT_USER_OAUTH_TOKEN,
  CHANNEL_TO_POST,
  TIME_OFFSET,
  AIRTABLE_BASE_NAME,
  AIRTABLE_VIEW_NAME,
} = require("./config");
const FIELDS = {
  description: "Description",
  communicationChannel: "Communication channel",
  date: "Date",
  audienceSize: "Audience size",
  offerType: "Offer type",
  reminderCreated: "Reminder created",
  deleteRecord: "Delete record",
};
const SLACK_WEB = new WebClient(SLACK_BOT_USER_OAUTH_TOKEN);
const AIRTABLE = new Airtable({ apiKey: AIRTABLE_API_KEY }).base(
  AIRTABLE_BASE_ID
);

const numberWithCommas = (x) => {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};

const deleteAirtableRecords = async (recordId) => {
  AIRTABLE(AIRTABLE_BASE_NAME).destroy([recordId], (err, deletedRecords) => {
    if (err) {
      console.error(err);
      return;
    }
    console.log(deletedRecords);
  });
};

const deleteSlackScheduledMessage = async (recordId) => {
  const scheduledMessages = await listScheduledSlackMessages();
  const scheduledMessagesArray = scheduledMessages.scheduled_messages;

  scheduledMessagesArray.find((message) => {
    if (message.text === recordId) {
      deleteScheduledSlackMessage(message.id);
    }
  });
};

const deleteRecords = (recordId) => {
  deleteAirtableRecords(recordId);
  deleteSlackScheduledMessage(recordId);
};

const scheduleSlackMessage = async (message) => {
  const result = await SLACK_WEB.chat.scheduleMessage({
    token: SLACK_BOT_USER_OAUTH_TOKEN,
    channel: CHANNEL_TO_POST,
    post_at: message.unixTimeStamp,
    text: message.text, // fallback requested by API, we're going to use this to delete records easily
    blocks: message.blocks,
  });
  console.log("SCHEDULE SLACK MESSAGE");
  console.log(result);
};

const postSlackMessage = async (message) => {
  const payload = {
    token: SLACK_BOT_USER_OAUTH_TOKEN,
    channel: CHANNEL_TO_POST,
    text: message.text, // fallback requested by API, we're going to use this to delete records easily
  };
  if (message.blocks) {
    payload.blocks = message.blocks;
  }

  const result = await SLACK_WEB.chat.postMessage(payload);
  console.log("POST SLACK MESSAGE");
  console.log(result);
};

const listScheduledSlackMessages = async () => {
  const result = await SLACK_WEB.chat.scheduledMessages.list({
    token: SLACK_BOT_USER_OAUTH_TOKEN,
    channel: CHANNEL_TO_POST,
  });

  console.log(result);

  return result;
};

const deleteScheduledSlackMessage = async (id) => {
  const result = await SLACK_WEB.chat.deleteScheduledMessage({
    token: SLACK_BOT_USER_OAUTH_TOKEN,
    channel: CHANNEL_TO_POST,
    scheduled_message_id: id,
  });

  console.log("DELETE SCHEDULED SLACK MESSAGE");
  console.log(result);
};

const getSlackTemplate = (offer) => {
  const blocks = [
    {
      type: "header",
      text: {
        type: "plain_text",
        text: ":calendar: Offer Scheduled",
      },
    },
    {
      type: "context",
      elements: [
        {
          type: "plain_text",
          emoji: true,
          text: `Date: ${offer.formattedDateTime}`,
        },
      ],
    },
    {
      type: "divider",
    },
    {
      type: "section",
      text: {
        type: "mrkdwn",
        text: `*${offer.description}*`,
      },
    },
    {
      type: "divider",
    },
  ];

  if (offer.audienceSize) {
    blocks.push({
      type: "section",
      text: {
        type: "mrkdwn",
        text: `*Audicence size:* ${numberWithCommas(offer.audienceSize)}`,
      },
    });
  }
  if (offer.offerType) {
    blocks.push({
      type: "section",
      text: {
        type: "mrkdwn",
        text: `*Offer type:* ${offer.offerType}`,
      },
    });
  }
  if (offer.communicationChannel) {
    blocks.push({
      type: "section",
      text: {
        type: "mrkdwn",
        text: `*Communication channel:* ${offer.communicationChannel}`,
      },
    });
  }

  blocks.push({
    type: "divider",
  });

  return blocks;
};

const updateAirtableRecords = async (recordsToBeUpdated) => {
  const recordsLength = recordsToBeUpdated.length;
  await postSlackMessage({
    text: `Scheduled ${recordsLength} message${
      recordsLength > 1 ? "s" : ""
    } for upcoming coupon events`,
    blocks: null,
  });

  // Sets the "Reminder created" property to true
  return new Promise((res, rej) => {
    AIRTABLE(AIRTABLE_BASE_NAME).update(recordsToBeUpdated, (err, _records) => {
      if (err) {
        rej(err);
      } else {
        res();
      }
    });
  });
};

(() => {
  const fetchData = async () => {
    // NOTE: Only pulling offers from the "pending-offers" view which already filters all past offers

    const recordsToBeUpdated = [];

    await new Promise((res, rej) => {
      AIRTABLE(AIRTABLE_BASE_NAME)
        .select({
          fields: [
            FIELDS.description,
            FIELDS.communicationChannel,
            FIELDS.date,
            FIELDS.audienceSize,
            FIELDS.offerType,
            FIELDS.deleteRecord,
          ],
          view: AIRTABLE_VIEW_NAME,
        })
        .eachPage(
          (records, processNextPage) => {
            // This function (`page`) will get called for each page of records.
            records
              .reduce(
                (p, record) =>
                  p.then(async () => {
                    const deleteRecord = record.get(FIELDS.deleteRecord);
                    const offerDate = record.get(FIELDS.date);
                    const formattedDateTime = new Date(
                      offerDate
                    ).toLocaleString();
                    const notificationTime =
                      Date.parse(offerDate) / 1000 - TIME_OFFSET;

                    const offer = {
                      text: record.id,
                      description: record.get(FIELDS.description),
                      communicationChannel: record.get(
                        FIELDS.communicationChannel
                      ),
                      audienceSize: record.get(FIELDS.audienceSize),
                      offerType: record.get(FIELDS.offerType),
                      unixTimeStamp: notificationTime,
                      formattedDateTime,
                    };

                    if (deleteRecord === "Delete") {
                      deleteRecords(record.id);
                    }

                    // postSlackMessage({
                    //   ...offer,
                    //   blocks: getSlackTemplate(offer),
                    // });
                    scheduleSlackMessage({
                      ...offer,
                      blocks: getSlackTemplate(offer),
                    });

                    // Creating structure to update Airtable after reminders are set
                    recordsToBeUpdated.push({
                      id: record.id,
                      fields: {
                        [FIELDS.reminderCreated]: true,
                      },
                    });
                  }),
                Promise.resolve()
              )
              .then(
                () => {
                  if (recordsToBeUpdated.length > 0) {
                    updateAirtableRecords(
                      recordsToBeUpdated.slice(0, recordsToBeUpdated.length)
                    ).then(
                      () => {
                        processNextPage();
                      },
                      (err) => {
                        rej(err);
                      }
                    );
                  } else {
                    processNextPage();
                  }
                },
                async (err) => {
                  if (recordsToBeUpdated.length > 0) {
                    updateAirtableRecords(
                      recordsToBeUpdated.slice(0, recordsToBeUpdated.length)
                    ).then(
                      () => {
                        rej(err);
                      },
                      (updateError) => {
                        rej(updateError);
                      }
                    );
                  } else {
                    rej(err);
                  }
                }
              );
          },
          (error) => {
            if (error) {
              rej(error);
            } else {
              res();
            }
          }
        );
    });
  };

  fetchData();

  // postSlackMessage(offer);
  // listScheduledSlackMessages();
  // deleteScheduledSlackMessage("Q026YG3E5RB");
})();
